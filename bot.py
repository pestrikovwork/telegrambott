from telebot import apihelper
import telebot
from telebot import types
import mysql.connector
from mysql.connector import errorcode
import os

apihelper.proxy = {'https':'socks5://149.56.27.45:1080'} #Подлкючаем прокси

bot = telebot.TeleBot('877327195:AAFjeU4UZxknQDU4FT6v19J4gxVJ8xXMRcw') #Подключаемся к боту
    
isRunning = False
@bot.message_handler(commands=['start'])
def start(message):
    global isRunning
    if not isRunning:
        try:
            connectDB = mysql.connector.connect(user='root', password='1234', host='localhost', database='test')
            cursor = connectDB.cursor()
            query = ("SELECT status FROM test.users WHERE users.id_user = {}".format(message.from_user.id))
            cursor.execute(query)
            for row in cursor:
                s = row[0]
                if s == "Активен":
                    bot.register_next_step_handler(message, start_menu)
                elif s == "Отписан":
                    del_user = ("UPDATE `test`.`users` SET `status` = 'Активен' WHERE (`id_user` = {})".format(message.from_user.id))
                    cursor.execute(del_user)
                    connectDB.commit()
                    cursor.close()
                    connectDB.close()
                    bot.register_next_step_handler(message, start_menu)
                else:
                    add_user = """INSERT INTO `test`.`users` (`id_user`, `user_name`, `name`, `last_name`, `status`) VALUES (%s, %s, %s, %s, %s);"""
                    data_user = (message.from_user.id, message.from_user.username, message.from_user.first_name, message.from_user.last_name, 'Активен')
                    cursor.execute(add_user, data_user)
                    connectDB.commit()
                    cursor.close()
                    connectDB.close()
                    bot.register_next_step_handler(message, start_menu)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        else:
            connectDB.close()

def start_menu(message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1Services = types.KeyboardButton('Услуги')
    btn2Records = types.KeyboardButton('Мои записи')
    btn3Contacts = types.KeyboardButton('Контакты')
    btn4Status = types.KeyboardButton('Отписаться')
    keyboard.add(btn1Services, btn2Records, btn3Contacts, btn4Status)
    msg = bot.send_message(message.chat.id, 'Выберите раздел', reply_markup=keyboard)
    isRunning = True
    bot.register_next_step_handler(msg, get_msg)

def get_msg(message):
    msg = message.text
    if msg == "Услуги":
        bot.register_next_step_handler(message, get_services)
    elif msg == "Мои записи":
        bot.register_next_step_handler(message, get_records)
    elif msg == "Контакты":
        bot.register_next_step_handler(message, get_contacts)
    elif msg == "Отписаться":
        bot.register_next_step_handler(message, get_status)
    else:
        msgError = bot.send_message(message.chat.id, 'Выберите из меню: ')
        bot.register_next_step_handler(msgError, get_msg)

def get_status(message):
    try:
            connectDB = mysql.connector.connect(user='root', password='1234', host='localhost', database='test')
            cursor = connectDB.cursor()
            del_user = ("UPDATE `test`.`users` SET `status` = 'Отписан' WHERE (`id_user` = {})".format(message.from_user.id))
            cursor.execute(del_user)
            connectDB.commit()
            cursor.close()
            connectDB.close()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connectDB.close()
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton('/start')
    keyboard.add(btn1)
    bot.send_message(message.chat.id, "Возвращайтесь. Для этого напишите команду /start")
        
def get_services(message):
    photo = open('C:/Users/Vadim/Desktop/bot/1.jpg', 'rb')
    bot.send_photo(message.chat.id, photo, "1) Ремонт телефонов\nОписание: Качественно и быстро проведем диагностику телефона и устраним все повреждения")
    photo = open('C:/Users/Vadim/Desktop/bot/2.jpg', 'rb')
    bot.send_photo(message.chat.id, photo, "2) Ремонт компьютеров\nОписание: Качественно и быстро проведем диагностику компьютера и устраним все повреждения")
    photo = open('C:/Users/Vadim/Desktop/bot/3.png', 'rb')
    bot.send_photo(message.chat.id, photo, """3) Настройка программ\nОписание: Установка ОС, офисного и антивирусного ПО, а также настйрока уже существующих программ, драйверов, утилит""")
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    itembtn1 = types.KeyboardButton(text='Ремонт телефонов')
    itembtn2 = types.KeyboardButton(text='Ремонт компьютеров')
    itembtn3 = types.KeyboardButton(text='Установка и найстройка ПО')
    keyboard.add(itembtn1, itembtn2, itembtn3)
    msg = bot.send_message(message.chat.id, 'Выберите услугу: ', reply_markup=keyboard)
    bot.register_next_step_handler(msg, get_servicesSubsection)

def get_records(message):
    try:
        connectDB = mysql.connector.connect(user='root', password='1234', host='localhost', database='test')
        cursor = connectDB.cursor()
        query = ("SELECT services, subsection, date_record, time_record FROM test.records WHERE records.id_user = {}".format(message.from_user.id))
        user = str(message.from_user.id)
        cursor.execute(query)
        for (services, subsection, date, time) in cursor:
            bot.send_message(message.from_user.id, "Вы записались на услугу: %s\nПодраздел: %s\nДата: %s\nВремя: %s" % (services, subsection, date, time))
        cursor.close()
        connectDB.close()
        bot.register_next_step_handler(message, start)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connectDB.close()
    isRunning = False    

def get_contacts(message):
    photo = open('C:/Users/Vadim/Desktop/bot/4.png', 'rb')
    bot.send_photo(message.from_user.id, photo, "ООО Ремонт РБ\nг. Уфа, ул. Ленина 105\nТелефон: 8 (347) 229-75-75")
    bot.register_next_step_handler(message, start)
    isRunning = False

isRunningSub = False
services = ''

def get_servicesSubsection(message):
    global services
    global isRunningSub
    services = msg = message.text
    if not isRunningSub:
        if msg == 'Ремонт телефонов':
            keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
            itembtn1 = types.KeyboardButton(text='Диагностика')
            itembtn2 = types.KeyboardButton(text='Ремонт деталей')
            itembtn3 = types.KeyboardButton(text='Программный ремонт')
            keyboard.add(itembtn1, itembtn2, itembtn3)
            msg = bot.send_message(message.chat.id, 'Выберите услугу: ', reply_markup=keyboard)
            isRunningSub = True
            bot.register_next_step_handler(msg, get_servicesPhone)
        elif msg == 'Ремонт компьютеров':
            keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
            itembtn1 = types.KeyboardButton(text='Диагностика')
            itembtn2 = types.KeyboardButton(text='Ремонт деталей')
            itembtn3 = types.KeyboardButton(text='Чистка')
            keyboard.add(itembtn1, itembtn2, itembtn3)
            msg = bot.send_message(message.chat.id, 'Выберите услугу: ', reply_markup=keyboard)
            isRunningSub = True
            bot.register_next_step_handler(msg, get_servicesComputer)
        elif msg == 'Установка и найстройка ПО':
            keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
            itembtn1 = types.KeyboardButton(text='Установка ОС')
            itembtn2 = types.KeyboardButton(text='Установка и настройка офисных программ')
            itembtn3 = types.KeyboardButton(text='Установка и настройка драйверов и утилит')
            keyboard.add(itembtn1, itembtn2, itembtn3)
            msg = bot.send_message(message.chat.id, 'Выберите услугу: ', reply_markup=keyboard)
            isRunningSub = True
            bot.register_next_step_handler(msg, get_servicesSoft)
        else:
            msgError = bot.send_message(message.chat.id, 'Выберите из меню: ')
            bot.register_next_step_handler(msgError, get_servicesSubsection)

servicesSection = ''

def get_servicesPhone(message):
    global servicesSection
    servicesSection = msg = message.text
    if msg == 'Диагностика':
        bot.register_next_step_handler(message, get_date)
    elif msg == 'Ремонт деталей':
        bot.register_next_step_handler(message, get_date)
    elif msg == 'Программный ремонт':
        bot.register_next_step_handler(message, get_date)
    else:
        msgError = bot.send_message(message.chat.id, 'Выберите из меню: ')
        bot.register_next_step_handler(msgError, get_servicesPhone)

def get_servicesComputer(message):
    global servicesSection
    servicesSection = msg = message.text
    msg = message.text
    if msg == 'Диагностика':
        bot.register_next_step_handler(message, get_date)
    elif msg == 'Ремонт деталей':
        bot.register_next_step_handler(message, get_date)
    elif msg == 'Чистка':
        bot.register_next_step_handler(message, get_date)
    else:
        msgError = bot.send_message(message.chat.id, 'Выберите из меню: ')
        bot.register_next_step_handler(msgError, get_servicesComputer)

def get_servicesSoft(message):
    global servicesSection
    servicesSection = msg = message.text
    msg = message.text
    if msg == 'Установка ОС':
        bot.register_next_step_handler(message, get_date)
    elif msg == 'Установка и настройка офисных программ':
        bot.register_next_step_handler(message, get_date)
    elif msg == 'Установка и настройка драйверов и утилит':
        bot.register_next_step_handler(message, get_date)
    else:
        msgError = bot.send_message(message.chat.id, 'Выберите из меню: ')
        bot.register_next_step_handler(msgError, get_servicesComputer)

def get_date(message):
    markup = types.ReplyKeyboardRemove(selective=False)
    msg = bot.send_message(message.chat.id, "Укажите удобную для вас дату")
    bot.register_next_step_handler(msg, get_time)

date = ''
isRunningTime = False

def get_time(message):
    global date
    global isRunningTime
    date = message.text
    if not isRunningTime:
        keyboard = types.ReplyKeyboardMarkup(row_width=4, resize_keyboard=True)
        itembtn1 = types.KeyboardButton(text='10:00')
        itembtn2 = types.KeyboardButton(text='11:00')
        itembtn3 = types.KeyboardButton(text='12:00')
        itembtn4 = types.KeyboardButton(text='13:00')
        itembtn5 = types.KeyboardButton(text='14:00')
        itembtn6 = types.KeyboardButton(text='15:00')
        itembtn7 = types.KeyboardButton(text='16:00')
        itembtn8 = types.KeyboardButton(text='17:00')
        keyboard.add(itembtn1, itembtn2, itembtn3, itembtn4, itembtn5, itembtn6, itembtn7, itembtn8)
        msg = bot.send_message(message.from_user.id, "В какой время вам удобно было бы подъехать? Выбирайте из предложенных или напишите когда вам удобно.", reply_markup=keyboard)
        isRunningTime = True
        bot.register_next_step_handler(msg, get_number)

time = ''

def get_number(message):
    global time
    time = message.text
    msg = bot.send_message(message.from_user.id, "Укажите номер телефона")
    bot.register_next_step_handler(msg, get_end)

number = ''

def get_end(message):
    global number
    global isRunning
    number = message.text
    if not number.isdigit():
        msg = bot.send_message(message.from_user.id, "Номер должен быть числом! Введите номер еще раз")
        bot.register_next_step_handler(msg, get_end)
    else:
        try:
            connectDB = mysql.connector.connect(user='root', password='1234', host='localhost', database='test')
            cursor = connectDB.cursor()
            add_records = """INSERT INTO `test`.`records` (`id_user`, `services`, `subsection`, `date_record`, `time_record`, `phone`) VALUES (%s, %s, %s, %s, %s, %s);"""
            data_records = (message.from_user.id, services, servicesSection, date, time, number)
            cursor.execute(add_records, data_records)
            connectDB.commit()
            cursor.close()
            connectDB.close()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        else:
            connectDB.close()
        bot.send_message(message.from_user.id, "Спасибо, будем вас ждать!")
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1Services = types.KeyboardButton('Услуги')
        btn2Records = types.KeyboardButton('Мои записи')
        btn3Contacts = types.KeyboardButton('Контакты')
        btn4Status = types.KeyboardButton('Отписаться')
        keyboard.add(btn1Services, btn2Records, btn3Contacts, btn4Status)
        msg = bot.send_message(message.chat.id, 'Выберите раздел', reply_markup=keyboard)
        bot.register_next_step_handler(msg, get_msg)
        isRunning = False

bot.polling(none_stop=True, interval=0)
